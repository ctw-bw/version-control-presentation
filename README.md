# README #

This is the presentation I gave to the ArmWorks group to say why version control is good

### How is this set up? ###

The presentation was written in [Markdown language](https://bitbucket.org/tutorials/markdowndemo), and uses [Reveal.js](http://www.reveal.js) to make an HTML page. To go from markdown to HTML I'm using [Pandoc][].

To build the presentation yourself, install [Pandoc][], and run `make.bat`.

[Pandoc]: http://pandoc.org